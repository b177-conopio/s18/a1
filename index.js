// 3. Create a trainer object using object literals. 4. Initialize/add the following trainer object properties
let trainer = {
	name: 'Ash Ketchum', 
	age: 10, 
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasur'],
	friends: {
		hoenn: ['May','Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log('Pikachu! I choose you!');
	}
}

console.log(trainer)

// 6. Access the trainer object properties using dot and square bracket notation.
console.log('Result of dot notation: ');
console.log(trainer.name);

console.log('Result of square bracket notation: ');
console.log(trainer['pokemon']);

// 7. Invoke/call the trainer talk object method.
console.log('Result of talk method: ')
trainer.talk();

// 8. Create a constructor for creating a pokemon with the following properties
function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

// 10. Create a tackle method
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		let newHealth = target.health - this.attack;
		console.log(target.name + "'s health is now reduced to " + newHealth);
		if(target.health - this.attack){
			target.health = newHealth
		}
		if(newHealth <= 0) {
			target.faint();
		}
	}
// 11. Create a faint method that will print out a message of targetPokemon has fainted.
	this.faint = function(){
		console.log(this.name + ' has fainted');
	}
}
// 9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
let pikachu = new Pokemon('Pikachu', 12);
let geodude = new Pokemon('Geodude', 8);
let mewtwo = new Pokemon('Mewtwo', 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);